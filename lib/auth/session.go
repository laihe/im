package auth

import (
	"github.com/gogf/gf/net/ghttp"
)

type session struct{}

var (
	Session = new(session)
)

const (
	sessionKey = `session.auth.user`
)

// UserLogin 用户登录
func (*session) UserLogin(r *ghttp.Request, user *User) error {
	return r.Session.Set(sessionKey, user)
}

// User 获取用户信息
func (*session) User(r *ghttp.Request) *User {
	if user, ok := r.Session.Get(sessionKey).(*User); ok {
		return user
	}
	return nil
}
