package http

import (
	"github.com/gogf/gf/net/ghttp"
)

// PageMeta 分页元数据
type PageMeta struct {
	TotalSize   int `json:"total"`        // 总数
	TotalPage   int `json:"page"`         // 总页数
	PageSize    int `json:"page_size"`    // 每页数量
	CurrentPage int `json:"current_page"` // 当前页数
}

// PageData 分页数据
type PageData struct {
	Meta *PageMeta   `json:"meta"` // 分页数据
	Data interface{} `json:"data"` // 数据
}

// ResPage 输出分页信息
func ResPage(r *ghttp.Request, total int, data interface{}, messages ...string) {
	page, pageSize := Page(r)
	ResJsonExit(r, true, PageData{
		Meta: &PageMeta{
			TotalSize:   total,
			TotalPage:   total/pageSize + 1,
			PageSize:    pageSize,
			CurrentPage: page,
		},
		Data: data,
	}, 0, messages...)
}
