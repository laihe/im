// Package websocket
// 通用操作码，从1000开始，每组操作码预留1000容量
// OpC开头的为客户端发送，OpS开头的为服务器发送
package websocket

type MessageOpcode uint16 // 消息操作码

// 通用操作码
const (
	OpCommonConnOpen  MessageOpcode = iota + 1000 // 1000 {}                              打开新链接
	OpCommonConnClose                             // 1001 {}                              关闭连接
	OpCommonMessage                               // 1002 {type:string,message:string}    消息
)

// 授权
const (
	OpCAuthLogin       MessageOpcode = iota + 2000 // 2000 {token:string}                 授权验证
	OpCAuthAnonymous                               // 2001 {}                             匿名验证
	OpSAuthLogin                                   // 2002 {user}                         授权验证
	OpSAuthAnonymous                               // 2003 {session}                      匿名验证
	OpSAuthFailed                                  // 2004 {message:string}               授权失败
	OpSNeedLogin                                   // 2005 {}                             需要登录才能操作
	OpSSessionLevelLoW                             // 2006 {need_level:int}               链接权限不足
	OpCSessionLevel                                // 2007 {level:int}                    切换权限
	OpSSessionLevel                                // 2008 {level:int}                    切换权限
)
