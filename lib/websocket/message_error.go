package websocket

// Error server会捕获此错误，并给session发送消息
type Error struct {
	Opcode  MessageOpcode
	Message string      // 错误提示
	Data    interface{} // 错误中包含的数据
}

func NewError(opcode MessageOpcode, msg string, data ...interface{}) *Error {
	e := &Error{
		Opcode:  opcode,
		Message: msg,
	}
	if len(data) > 0 {
		e.Data = data[0]
	}

	return e
}

func (e *Error) Error() string {
	return e.Message
}
