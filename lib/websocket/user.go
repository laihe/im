package websocket

import (
	"sync"

	"websocket/lib/auth"
)

// User 用户信息
type User struct {
	*auth.User
	sessionLocker *sync.RWMutex       // session锁
	sessions      map[uint64]*Session // session集合
}

func NewUser(user *auth.User) *User {
	return &User{
		User:          user,
		sessionLocker: &sync.RWMutex{},
		sessions:      make(map[uint64]*Session),
	}
}

// AddSession 添加连接
func (u *User) AddSession(session *Session) {
	u.sessionLocker.Lock()
	defer u.sessionLocker.Unlock()

	u.sessions[session.Id] = session
}

// DelSession 删除session
func (u *User) DelSession(session *Session) {
	u.sessionLocker.Lock()
	defer u.sessionLocker.Unlock()

	delete(u.sessions, session.Id)
}

// GetSessions 所有session
func (u *User) GetSessions() map[uint64]*Session {
	u.sessionLocker.RLock()
	defer u.sessionLocker.RUnlock()

	return u.sessions
}
