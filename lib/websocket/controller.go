package websocket

import (
	"websocket/lib/auth"
)

type Controller struct{}

// Open 打开连接
func (*Controller) Open(s *Session, m *MessageReq) {
	Server().AddSession(s)
}

// Close 断开连接
func (*Controller) Close(s *Session, m *MessageReq) {
	Server().DelSession(s)

	if s.User != nil {
		s.User.DelSession(s)
		if len(s.User.GetSessions()) == 0 {
			Server().DelUser(s.User)
		}
	}
}

// Auth 授权
func (*Controller) Auth(s *Session, m *MessageReq) {
	if s.IsLogin() {
		s.SendMessageFromError(NewError(OpSAuthFailed, `用户已经登录`))
		return
	}

	token := m.Data.GetString(`token`)
	jwtUser, err := auth.Jwt.User(token)
	if err != nil {
		s.SendMessageFromError(NewError(OpSAuthFailed, err.Error()))
		return
	}

	user := Server().GetUser(jwtUser.Id)
	if user == nil {
		user = NewUser(jwtUser)
		Server().AddUser(user)
	} else {
		if len(user.GetSessions()) >= 5 {
			s.SendMessageFromError(NewError(OpSAuthFailed, `客户端数量过多`))
			return
		}
	}
	s.AuthUser(user)
	user.AddSession(s)
}

// Anonymous 匿名
func (*Controller) Anonymous(s *Session, m *MessageReq) {
	if !s.IsLogin() {
		s.Anonymous()
	} else {
		s.SendMessageFromError(NewError(OpSAuthFailed, `用户已经登录`))
	}
}
