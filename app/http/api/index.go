package api

import (
	"github.com/gogf/gf/net/ghttp"

	"websocket/lib/http"
)

type Index struct{}

func (*Index) Index(r *ghttp.Request) {
	http.ResSuccess(r, `Hello World!`)
}
