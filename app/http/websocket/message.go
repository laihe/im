package websocket

import (
	"websocket/lib/websocket"
)

type Message struct{}

func (*Message) Open(s *websocket.Session, m *websocket.MessageReq) {
	s.SendTextMessage([]byte(`Hello World!`))
}
