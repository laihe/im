module websocket

require (
	github.com/bwmarrin/snowflake v0.3.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gogf/gf v1.16.6
	github.com/gorilla/websocket v1.4.2
	github.com/xuri/excelize/v2 v2.4.1
	go.uber.org/ratelimit v0.2.0
)

go 1.14
