package main

import (
	"github.com/gogf/gf/frame/g"

	_ "websocket/boot"
	"websocket/router"
)

func main() {
	router.RegisterRouter()
	router.RegisterWebsocket()

	g.Server().Run()
}
