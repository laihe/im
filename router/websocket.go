package router

import (
	"github.com/gogf/gf/frame/g"

	wsApi "websocket/app/http/websocket"
	wsLib "websocket/lib/websocket"
)

type wsHandler struct {
	level  wsLib.SessionLevel
	opcode wsLib.MessageOpcode
	handle wsLib.MessageHandleFunc
}

// 注册操作码处理函数
func getHandlers() []*wsHandler {
	var handlers []*wsHandler

	var mc = new(wsApi.Message)
	handlers = append(handlers, []*wsHandler{
		{wsLib.SessionLevelConnect, wsLib.OpCommonConnOpen, mc.Open}, // test
	}...)

	return handlers
}

func RegisterWebsocket() {
	var server = wsLib.Server()

	// 消息处理
	for _, v := range getHandlers() {
		server.AddHandler(v.level, v.opcode, v.handle)
	}

	// 注册websocket路由
	g.Server().BindHandler(`/websocket`, server.HandleWebsocket)
}
