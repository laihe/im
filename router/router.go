package router

import (
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"

	"websocket/app/http/api"
)

func RegisterRouter() {
	s := g.Server()

	s.Group("/", func(group *ghttp.RouterGroup) {
		group.GET(`/`, new(api.Index).Index)
	})
}
